﻿namespace FizzBuzzWeb.Models
{
    using System.ComponentModel.DataAnnotations;
    using PagedList;
    using PagedList.Mvc;

    public class FizzBuzzModel
    {
        public IPagedList<string> FizzBuzzNumbers { get; set; }

        [Range(1, 1000, ErrorMessage = "The Number Should be Between 1 to 1000")]
        [Required]
        public int Input { get; set; }
    }
}