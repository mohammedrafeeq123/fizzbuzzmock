namespace FizzBuzzWeb.DependencyResolution
{
    using FizzBuzz.Services.Implementations;
    using FizzBuzz.Services.Interfaces;
    using StructureMap.Graph;
    using StructureMap;

    public static class IoC
    {
        public static IContainer Initialize()
        {
            ObjectFactory.Initialize(x =>
            {
                x.Scan(scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                });             
                x.For<IFizzBuzzRule>().Use<FizzRule>();
                x.For<IFizzBuzzRule>().Use<BuzzRule>();
                x.For<IFizzBuzzServices>().Use<FizzBuzzService>();
                x.For<IDayProvider>().Use<DayProvider>();
             
            });
            return ObjectFactory.Container;
        }
    }
}