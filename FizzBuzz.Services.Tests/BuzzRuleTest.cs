﻿namespace BusinessLogic
{
    using System;
    using FizzBuzz.Services.Constants;
    using FizzBuzz.Services.Implementations;
    using FizzBuzz.Services.Interfaces;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class BuzzRuleTests
    {
        private Mock<IDayProvider> dayProvider;

        [TestCase(1, false)]
        [TestCase(5, true)]
        [TestCase(7, false)]
        public void IsValid_ShouldReturnTrueIfSatisfied(int value, bool expectedResult)
        {
            this.dayProvider = new Mock<IDayProvider>();
            var result = new BuzzRule(this.dayProvider.Object);
            var actualResult = result.IsValid(value);
            Assert.AreEqual(actualResult, expectedResult);
        }

        [TestCase(false, Constants.Buzz)]
        [TestCase(true, Constants.Wuzz)]
        public void GetValue_WuzzIfWednesday_OtherWise_Buzz(bool isDayProviderSatisfied, string expectedResult)
        {
            this.dayProvider = new Mock<IDayProvider>();
            var setUpValue = this.dayProvider.Setup(x => x.IsValid(DateTime.Now.DayOfWeek)).Returns(isDayProviderSatisfied);
            var result = new BuzzRule(this.dayProvider.Object);
            var actualResult = result.GetContent();
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
