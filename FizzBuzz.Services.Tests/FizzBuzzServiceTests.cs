﻿namespace FizzBuzz.Services.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using FizzBuzz.Services.Implementations;
    using NUnit.Framework;
    using FizzBuzz.Services.Interfaces;
    using Moq;
    using NUnit.Framework;
    

    public class FizzBuzzServiceTests
    {
        private Mock<IFizzBuzzRule> fizzRule;
        private Mock<IFizzBuzzRule> buzzRule;

        private Mock<IFizzBuzzRule> fizzBuzzRule;

        private static object[] fizzBuzzDataSource =
        {
            new object[] { 3, new List<string> {"1", "2", "fizz"}},
            new object[] { 5, new List<string> {"1", "2", "fizz", "4", "buzz" }},
            new object[] { 15, new List<string> {"1", "2", "fizz", "4", "buzz", "fizz", "7", "8", "fizz", "buzz", "11", "fizz", "13", "14", "fizz buzz"}},
        };

        [TestCaseSource("fizzBuzzDataSource")]
        public void Service_Should_Return_FizzBuzzResult_BasedOnValuesProvided(int value, List<string> expected)
        {
            this.fizzBuzzRule = new Mock<IFizzBuzzRule>();
            this.fizzRule = new Mock<IFizzBuzzRule>();
            this.buzzRule = new Mock<IFizzBuzzRule>();
            this.fizzRule.Setup(x => x.IsValid(3)).Returns(true);
            this.fizzRule.Setup(x => x.IsValid(6)).Returns(true);
            this.fizzRule.Setup(x => x.IsValid(9)).Returns(true);
            this.fizzRule.Setup(x => x.IsValid(12)).Returns(true);
            this.fizzRule.Setup(x => x.GetContent()).Returns("fizz");

            this.buzzRule.Setup(y => y.IsValid(5)).Returns(true);
            this.buzzRule.Setup(y => y.IsValid(10)).Returns(true);
            this.buzzRule.Setup(x => x.GetContent()).Returns("buzz");

            this.fizzBuzzRule.Setup(z => z.IsValid(15)).Returns(true);
            this.fizzBuzzRule.Setup(z => z.GetContent()).Returns("fizz buzz");
            var service = new FizzBuzzService(new List<IFizzBuzzRule>() { this.fizzRule.Object, this.buzzRule.Object, this.fizzBuzzRule.Object });

            var result = service.GetResult(value);

            Assert.AreEqual(result, expected);
        }

        [Test]
        public void ThreeAndFiveDivisionTest(int value, string expected)
        {
            this.fizzBuzzRule = new Mock<IFizzBuzzRule>();
            this.fizzBuzzRule.Setup(x => x.IsValid(value)).Returns(true);
            this.fizzBuzzRule.Setup(y => y.GetContent()).Returns("buzz wuzz");
            FizzBuzzService factory = new FizzBuzzService(new List<IFizzBuzzRule>() { this.fizzBuzzRule.Object });
            var result = factory.GetResult(15);
            var actual = result.ElementAt(14);
            Assert.AreEqual(actual, expected);
        }




    }
}
