﻿namespace BusinessLogic
{
    using System;
    using FizzBuzz.Services.Constants;
    using FizzBuzz.Services.Implementations;
    using FizzBuzz.Services.Interfaces;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class FizzRuleTests
    {
        private Mock<IDayProvider> dayProvider;

        [TestCase(1, false)]
        [TestCase(3, true)]
        [TestCase(5, false)]
        public void IsValid_ShouldReturnTrueIfSatisfied(int value, bool expectedResult)
        {
            this.dayProvider = new Mock<IDayProvider>();
            var result = new FizzRule(this.dayProvider.Object);

            var actualResult = result.IsValid(value);

            Assert.AreEqual(actualResult, expectedResult);
        }

        [TestCase(false, Constants.Fizz)]
        [TestCase(true, Constants.Wizz)]
        public void GetValue_WizzIfWednesday_OtherWise_Fizz(bool isDayProviderSatisfied, string expectedResult)
        {
            this.dayProvider = new Mock<IDayProvider>();
            this.dayProvider.Setup(x => x.IsValid(DateTime.Now.DayOfWeek)).Returns(isDayProviderSatisfied);
            var result = new FizzRule(this.dayProvider.Object);

            var actualResult = result.GetContent();
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}