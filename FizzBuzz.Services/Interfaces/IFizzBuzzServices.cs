﻿namespace FizzBuzz.Services.Interfaces
{
    using System.Collections.Generic;

    public interface IFizzBuzzServices
    {
        IEnumerable<string> GetResult(int number);
    }
}
