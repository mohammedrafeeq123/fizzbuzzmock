﻿namespace FizzBuzz.Services.Implementations
{
    using System;
    using FizzBuzz.Services.Constants;
    using FizzBuzz.Services.Interfaces;
    using FizzBuzz.Services.Constants;

    public class DayProvider : IDayProvider
    {
        public bool IsValid(DayOfWeek dayOfWeek)
        {
            return dayOfWeek == Constants.DayToCheck;
        }
    }
}
