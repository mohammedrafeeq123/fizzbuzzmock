﻿namespace FizzBuzz.Services.Implementations
{
    using System;
    using FizzBuzz.Services.Constants;
    using FizzBuzz.Services.Interfaces;

    public class FizzRule : IFizzBuzzRule
    {
        private readonly IDayProvider dayProvider;

        public FizzRule(IDayProvider dayProvider)
        {
            this.dayProvider = dayProvider;
        }

        public bool IsValid(int number)
        {
            return number % Constants.FizzDivisor == 0;
        }

        public string GetContent()
        {
            return this.dayProvider.IsValid(DateTime.Now.DayOfWeek) ? Constants.Wizz : Constants.Fizz;
        }
    }
}
