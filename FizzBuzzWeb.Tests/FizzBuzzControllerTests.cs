﻿namespace FizzBuzzWeb.Tests
{
    using System.Web.Mvc;
    using System.Collections.Generic;
    using FizzBuzzWeb.Controllers;
    using FizzBuzz.Services.Interfaces;
    using FizzBuzzWeb.Models;
    using Moq;
    using NUnit.Framework;


    [TestFixture]
    public class FizzBuzzControllerTests
    {
        private Mock<IFizzBuzzServices> fizzBuzz;

        private static object[] validationDataSource =
        {
            new object[] { -1, new List<string> { "The Number Should be Between 1 to 1000" } },
            new object[] { 1034, new List<string> { "The Number Should be Between 1 to 1000" } },
        };

        [Test]
        public void GetIndexTest()
        {
            FizzBuzzController controller = new FizzBuzzController(this.fizzBuzz.Object);
            var result = controller.Index(new FizzBuzzModel()) as ViewResult;

            var actualResult = result.ViewName;

            Assert.AreEqual("Index", actualResult);
        }

        [Test]
        public void PostIndexTest()
        {
            var expectedResult = new List<string> { "1", "2", "fizz", "4", "buzz" };
            this.fizzBuzz.Setup(x => x.GetResult(It.IsAny<int>())).Returns(expectedResult);
            var controller = new FizzBuzzController(this.fizzBuzz.Object);

            var result = controller.Index(new FizzBuzzModel() { Input = 5 }) as ViewResult;
            var model = result.Model as FizzBuzzModel;

            var actualResult = new List<string>(model.FizzBuzzNumbers);
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestCaseSource("validationDataSource")]
        public void ModelStateValidationTest(int value, List<string> expectedResult)
        {
            this.fizzBuzz.Setup(x => x.GetResult(It.IsAny<int>())).Returns(expectedResult);
            var controller = new FizzBuzzController(this.fizzBuzz.Object);

            var output = controller.Index(new FizzBuzzModel() { Input = value }) as ViewResult;
            var model = output.Model as FizzBuzzModel;
            var result = model.FizzBuzzNumbers;

            var actualResult = result;
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestCaseSource("paginationDataSource")]
        public void PaginationTest(int value, List<string> expectedResult)
        {
            this.fizzBuzz.Setup(x => x.GetResult(It.IsAny<int>())).Returns(expectedResult);
            FizzBuzzController controller = new FizzBuzzController(this.fizzBuzz.Object);

            var output = controller.Navigate(value, 1) as ViewResult;
            var model = output.Model as FizzBuzzModel;

            var actualResult = new List<string>(model.FizzBuzzNumbers);
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
